---
layout: post
title: "Setting up IntelliJ/GoLand for a Go app on Google App Engine"
image: img/Gopher.jpg
author: [Michael]
tags: ["google-cloud","golang","google-app-engine","jetbrains","intellij","goland"]
date: "2018-02-11T10:22:10.494Z"
draft: false
---

I’m starting to explore Go (aka Golang) and I have heard a lot that suggests Google App Engine could be a neat solution for deploying very small, simple apps, such as my little experiments. Here’s a summary of how to get set up for development on this stack if, like me, IntelliJ/GoLand is your IDE of choice.

### Google Cloud SDK
First you’ll need to [install the Google Cloud SDK](https://cloud.google.com/sdk/docs/). (This provides the `gcloud` command, so if you’ve ever run that, you already have this installed.)

The tools for working with Go applications on Google App Engine (GAE) can now be installed with `gcloud components install app-engine-go` (for yum/apt package names [see here](https://cloud.google.com/sdk/docs/components#external_package_managerswzxhzdk13wzxhzdk14)). This includes a [local development server](https://cloud.google.com/appengine/docs/standard/go/tools/using-local-server) which lets you simulate your application running in App Engine, including enforcing some of the sandbox restrictions and simulating Datastore, Task Queues and some other services.

### Go workspace
The Go toolchain has really strong opinions about how you should structure your projects, described in [How to write Go code](https://golang.org/doc/code.html#Organization). In summary, all your Go projects must live under one common directory, which you must specify with an environment variable `GOPATH` (or use the default `~/go`).
Suppose you want to play around with the project at https://github.com/GoogleCloudPlatform/appengine-guestbook-go. You will need to clone this into `$GOPATH/src/github.com/GoogleCloudPlatform/appengine-guestbook-go` for things to work correctly.

### IntelliJ/GoLand
[IntelliJ IDEA Ultimate](https://www.jetbrains.com/idea/) is my IDE of choice for JVM languages and it is easy to install the [Go plugin](https://plugins.jetbrains.com/plugin/9568-go) which gets you all the functionality of [GoLand](https://www.jetbrains.com/go/), JetBrains’ new Go IDE. While you could just write your Go code and then use the standard AppEngine tools, you may prefer to run your app in the local development server using the IDE’s run configurations.

Once in GoLand (or IDEA with the Go plugin) you can create a new project from your local repo, but you may need to specify your `GOPATH` in the Preferences under _Languages & Frameworks → Go → GOPATH_. I’ve set mine as the Global GOPATH, but you can have per–project settings if you prefer.

The only way I’ve found to select the right version of Go itself is under _Languages & Frameworks → Go → GOROOT_ where the dropdown list can be added to by clicking the **…** button to the right. For an App Engine project, you’ll want to find the Go tools installed with the google-cloud-sdk components. Don’t know where this is? Here’s a handy command[^2]:
```bash
ls -1d $(dirname $(dirname $(which gcloud)))/platform/google_appengine/goroot*
```
On my machine I have `/Users/michael/Develop/google-cloud-sdk/platform/google_appengine/goroot-1.8`, so I’ve selected that. (There’s a goroot-1.6 as well, but I want to use the latest version.)

### Running your app in the GAE development server

Now, let’s assume you have a GAE-ready Go application like appengine-guestbook-go (for that repo, you’ll need to checkout one of the branches, such as `part1-helloworld`).

Go to _Run → Edit Configurations…_ and click **+** to Add New Configuration, selecting Go App Engine. Name the configuration, and then add an entry to the Environment, setting `APPENGINE_DEV_APPSERVER` to `<path-to-google-cloud-sdk>/bin/dev_appserver.py`.[^3]
Again, you need to know where you put your Google Cloud SDK is; here’s a useful command to find the path:
```bash
echo "$(dirname $(which gcloud))/dev_appserver.py"
```
On my machine this is `/Users/michael/Develop/google-cloud-sdk/bin/dev_appserver.py`.

If you want, you can specify a port and/or an admin port, but defaults will be used if you leave these blank. The **Config file** will be found automatically. 
You probably want to tick **Single instance only** at the top-right of the dialog.

Hit the ▶ (Run) button in your toolbar, and you should see some info in the Run console with links to your app as well as the admin server (this is quite interesting to look around):

```
GOROOT=/Users/michael/Develop/google-cloud-sdk/platform/google_appengine/goroot-1.8 #gosetup
GOPATH=/Users/michael/Develop/go #gosetup
/Users/michael/Develop/google-cloud-sdk/platform/google_appengine/goroot-1.8/bin/goapp serve . #gosetup
INFO     2018-02-11 10:09:59,686 devappserver2.py:105] Skipping SDK update check.
INFO     2018-02-11 10:09:59,815 api_server.py:301] Starting API server at: http://localhost:50412
INFO     2018-02-11 10:09:59,829 dispatcher.py:251] Starting module "default" running at: http://localhost:8080
INFO     2018-02-11 10:09:59,835 admin_server.py:116] Starting admin server at: http://localhost:8000
```

One really neat thing about this is that it will pick up code changes automatically, and recompile for you. You’ll see this in the log:

```
INFO     2018-02-11 10:13:02,336 module.py:412] [default] Detected file changes:
  /Users/michael/Develop/go/src/github.com/GoogleCloudPlatform/appengine-guestbook-go/hello.go
```

### Off you Go…

That’s all for now. I’m still learning and I hope to write some more interesting posts as I get further down this path.


[^1]: For Linux users, there are apt/yum repos, but if you use those then the <tt>gcloud components</tt> commands can’t be used to install and update additional components.

[^2]: This is probably not the neatest way of doing this, but it works.

[^3]: I found this detail in https://intellij-support.jetbrains.com/hc/en-us/community/posts/360000033390-Launching-golang-appengine-app. The JetBrains employee who responded seems to indicate that this would be fixed in some future version so that it would auto-discover `dev_appserver.py`.

---

_Image credit: <a href="https://commons.wikimedia.org/wiki/User:TLPIMAGES">TLPIMAGES</a>, <a href="https://commons.wikimedia.org/wiki/File:Gopher_DSC_0115_NO_LOGO.jpg">Gopher DSC 0115 NO LOGO</a>, <a href="https://creativecommons.org/licenses/by-sa/3.0/legalcode" rel="license">CC BY-SA 3.0</a>_
