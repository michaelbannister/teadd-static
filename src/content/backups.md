---
layout: post
title: "On the importance of backups"
image: img/Dds_tape_drive_01.jpg
author: [Michael]
tags: []
date: "2016-04-17T19:35:00.000Z"
featured: false
draft: false
---

This is the first time I’ve felt pleased that I haven’t actually written many posts here. My web host had a storage failure and lost all my data.

_“Surely an experienced IT professional like you had a backup strategy!?”_ I hear you ask.

Well… no. I had a local copy of the whole Ghost installation which I had copied back from the server after my first post, so I could mess about with the theme. But that’s all.

So, I’m ditching the web hosts I was using. Even before the incident, the server seemed to go down fairly frequently (albeit briefly) and although they were very responsive to support requests (including giving me SSH access and full control over my DNS) it was getting annoying. They’ve now said they won’t give out SSH access any more as they have reason to think someone messed things up through that route.

Instead, this blog is now running on a server in Google Cloud. The content is still stored directly on that server so as a first step I’ll be writing a backup script which I’ll run periodically from my local machine, at least after each post. After that, I’ll see if I can schedule something from the cloud server itself.

With a bit of luck and a little more determination, I’ll start posting a little more frequently from now on.

---

*[Image](https://commons.wikimedia.org/w/index.php?curid=94360) by [Robert Jacek Tomczak](http://commons.wikimedia.org/wiki/User:Rjt), licence [CC BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/)*
