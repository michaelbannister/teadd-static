---
layout: post
title: "Hello, World!"
image: img/callum-shaw-555357-unsplash.jpg
author: [Michael]
tags: []
date: "2016-02-06T10:55:00.000Z"
draft: false
excerpt: ''
---

Welcome to TeaDD, my blog about software craftsmanship.

My name is Michael Bannister, and I’m a lead developer in e‑commerce at a large UK retailer. I mainly work in Java, Groovy and JavaScript, but I’ve had a little experience of VB.NET (ugh) and just a teensy taste of Scala, Ruby, C, Standard ML, C#, Perl and (I can’t believe I’m admitting this in public) some COBOL.

So, why this blog?

In January 2016, I attended a great two-day [TDD Workshop](http://www.codemanship.co.uk/tdd.html), run by [@jasongorman](https://twitter.com/jasongorman), with a dozen or so colleagues. Highly recommended, not only for the actual TDD technique but also for a load of good arguments you can present back to your managers to explain why you should be doing this TDD thing.

Anyway, one of the outcomes of this workshop was that those who attended set up a weekly “coding club” where we get together to pair on a TDD kata, compare and discuss our approaches, and generally learn from each other while we practice writing the _simplest_ possible code that will make that failing test pass.

As bad luck would have it, I missed the first session and so I caught up a couple of days later. Rather than email my colleagues with my thoughts and comments, I decided to use this as material to kickstart a blog: something I’d been intending to do for some time.
