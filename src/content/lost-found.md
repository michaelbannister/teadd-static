---
layout: post
title: "lost+found"
author: [Michael]
tags: []
date: "2016-04-20T21:13:00.000Z"
draft: false
---

Thanks to a friend who had subscribed to this blog with an RSS reader, I’ve been able to restore my lost posts.

Thanks, Simon! :)

The comments facility will also be reinstated soon.
