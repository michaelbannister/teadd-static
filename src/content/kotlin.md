---
layout: post
title: "Kotlin"
image: img/kotlin_island.jpg
author: [Michael]
tags: ["kotlin"]
date: "2016-02-25T22:19:00.000Z"
draft: false
---

Back in February, [JetBrains released version 1.0 of their JVM language, Kotlin](https://blog.jetbrains.com/kotlin/2016/02/kotlin-1-0-released-pragmatic-language-for-jvm-and-android/). Read that blog for more on what Kotlin is, but their summary is:

>Kotlin is a pragmatic programming language for JVM and Android that combines OO and functional features and is focused on interoperability, safety, clarity and tooling support.

The Kotlin reference docs contain a [comparison to Scala](https://kotlinlang.org/docs/reference/comparison-to-scala.html) and “[Why a new language?](https://kotlinlang.org/docs/reference/faq.html#why-a-new-language)” in the FAQ which together, in my view, make it quite clear what the creators of Kotlin were trying to do: create a language with some of the benefits of Scala over Java, but “focused on interoperability, safety, clarity and tooling support”.[^1]

If you use Java I highly recommend you take a look. To get a flavour of why you might use it, the [idioms](https://kotlinlang.org/docs/reference/idioms.html) page is pretty good; to start learning to write Kotlin code, I think the best way to get a feel for it is to dive in with the [Kotlin Koans](https://kotlinlang.org/docs/tutorials/koans.html), which you can either clone from GitHub and solve in your IDE, or use the rather amazing in-browser experience at [try.kotlin.org](http://try.kotlinlang.org). (It even has Ctrl-Space autocompletion!)

[^1]: I don’t know much about Scala, but I understand that many people criticise it for lacking in some of these attributes.

---
_Image credit: photograph of [Kotlin Island, St Petersburg](http://wikimapia.org/597467/Kotlin-Island#/photo/3208540), by [vlad10531](http://wikimapia.org/#show=/user/349246/) is licensed under [CC-BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/)_
